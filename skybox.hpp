#pragma once

#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>
#include <glm/ext.hpp>

#include <context.hpp>

class skybox {
  GLuint vs,fs;
  GLuint vbo;
  GLuint vao;
  GLuint shader_programme;
  GLuint cubemapTexture;
  GLint skyboxView,skyboxProjection;

public:
  // The constructor will set the texture id in the context
  skybox(context &);
  void draw(const context &);
};
