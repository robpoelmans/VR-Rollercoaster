#include "rollercoaster-mechanics.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

rollercoaster_mechanics rollercoaster_mechanics::load(const std::string &path) {
    rollercoaster_mechanics m;
    m.running = false;
    m.velocity = 2;
    m.position = 4;
    std::ifstream coaster;
    coaster.open(path.c_str());
    if(!coaster.good()) {
        throw file_not_found_exception();
    }

    for(std::string str_line; std::getline(coaster, str_line);) {
        std::istringstream line(str_line);
        std::string flags;
        base_coordinate pos;
        line >> pos.x >> pos.y >> pos.z;
        line >> pos.dx >> pos.dy >> pos.dz;
        line >> flags;
        pos.flag = 0;
        for (char flag: flags) {
            switch (flag) {
            case 'Z':
                pos.flag |= segment_flag::z;
                break;
            case 'O':
                pos.flag |= segment_flag::o;
                break;
            case 'I':
                pos.flag |= segment_flag::i;
                break;
            default:
                std::cerr << "Unknown flag " << flag << std::endl;
            }
        }
        line >> pos.rot_begin;
        line >> pos.rot_end;
        m.track.push_back(pos);
    }
    m.precalculate_partial_lengths();

    return m;
}
