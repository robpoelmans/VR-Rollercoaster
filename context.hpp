#pragma once

#include <GL/glew.h>
#include <glm/matrix.hpp>

struct context {
    glm::mat4 camera_matrix;
    glm::mat4 view;
    glm::mat4 skybox_view;
    glm::mat4 projection;
    glm::ivec2 viewport_dimensions;
    glm::vec3 position;
    glm::vec3 lookat;
    glm::vec3 up;
    GLuint skybox_texture;
};
