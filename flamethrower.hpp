#pragma once

#include <glm/ext.hpp>

#include "context.hpp"

class flamethrower {
    static const int max_particles = 1024*100;

    float position[max_particles*3];
    glm::vec3 velocity[max_particles];
    float temperature[max_particles];

    double previous_time;

    GLuint vbo;
    GLuint temperature_buffer;

    GLuint vs,fs,ge;
    GLuint shader_programme;
    GLuint vao;

    // Uniforms
    GLuint u_mvp;

    void respawn(size_t i);
public:
    flamethrower();
    void update();
    void draw(const context &);
};
