#version 400

layout (points) in;
layout (triangle_strip, max_vertices = 6) out;

uniform mat4 mvp;

in vec3 vPosition[];
in float vTemperature[];

out float gTemperature;

const vec3 vertex_buffer_data[4] = vec3[](
  vec3(-0.01f, -0.0f, -0.01f),
  vec3(0.01f, -0.0f, -0.01f),
  vec3(-0.01f, 0.0f, 0.01f),
  vec3(0.01f, 0.0f, 0.01f)
);

const vec3 centre_of_fun = vec3(10, -38, 0);

void main() {
    if(vTemperature[0] < 800) return;
    gTemperature = vTemperature[0];

    for(uint i = 0; i < 4; ++i) {
        gl_Position = mvp*vec4(centre_of_fun+vPosition[0]+vertex_buffer_data[i], 1.0);
        EmitVertex ();
    }
    EndPrimitive();
}
