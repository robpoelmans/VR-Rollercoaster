#version 400

// 4 vertices per-primitive -- 2 for the line (1,2) and 2 for adjacency (0,3)
layout (lines) in;
// Standard fare for drawing lines
layout (triangle_strip, max_vertices = 128) out;

uniform mat4 mvp;

in vec3 vPosition[];
in vec3 vDerivative[];
in vec3 vBinormal[];

out vec3 p0;
out vec3 p1;
out vec3 m0;
out vec3 m1;

out vec3 dir;
out vec3 start;
out vec3 end;

const float track_diameter = 0.05;
const float track_width = 0.8;

// Emit a beam with track_diameter and two points
void emit_beam(vec3 begin, vec3 end) {
    vec3 direction = normalize(end-begin);

    // First find two vectors perpendicular to the direction
    vec3 h = vBinormal[0];
    vec3 v = cross(h, direction);

    float r = track_diameter/2 + 0.01;
    h *= r;
    v *= r;

    // Now emit the beam comprising the vertices
    vec3 verts[8] = vec3[](
        end + h + v,
        end - h + v,
        end + h - v,
        end - h - v,
        begin + h + v,
        begin - h + v,
        begin - h - v,
        begin + h - v
    );

    const uint elements[14] = uint[](
        3, 2, 6, 7, 4, 2, 0,
        3, 1, 6, 5, 4, 1, 0
    );

    for(uint i=0; i < 14-2; ++i) {
        for(uint j = 0; j < 3; ++j) {
            gl_Position = mvp*vec4(verts[elements[i+j]], 1.0);
            EmitVertex ();
        }
        EndPrimitive();
    }
}

void main (void) {
    p0 = vPosition[0];
    p1 = vPosition[1];
    m0 = vDerivative[0];
    m1 = vDerivative[1];

    vec3 binormal0 = vBinormal[0];
    vec3 binormal1 = vBinormal[1];

    // Left
    start = p0 - binormal0*0.5*track_width;
    end = p1 - binormal1*0.5*track_width;
    dir = end-start;

    emit_beam(start, end);

    // Right
    start = p0 + binormal0*0.5*track_width;
    end = p1 + binormal1*0.5*track_width;
    dir = end-start;

    emit_beam(start, end);
}
