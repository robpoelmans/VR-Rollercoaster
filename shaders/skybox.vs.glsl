#version 400
layout(location = 0) in vec3 position;

out vec3 TexCoords;

uniform mat4 skyboxView;
uniform mat4 skyboxProjection;

void main() {
  mat3 z_is_up = mat3(1,0,0,
                      0,0,1,
                      0,-1,0);
  vec4 pos = skyboxProjection * skyboxView * vec4(position,1.0f);
  gl_Position = pos.xyww;
  TexCoords = z_is_up * position;
}
