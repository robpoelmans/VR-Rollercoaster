#pragma once

#include <cassert>
#include <iostream>

#include <GL/glew.h>

extern unsigned char track_fs[];
extern unsigned char track_vs[];
extern unsigned char track_ge[];

extern unsigned char flamethrower_fs[];
extern unsigned char flamethrower_vs[];
extern unsigned char flamethrower_ge[];

extern unsigned char skybox_fs[];
extern unsigned char skybox_vs[];

inline void handle_shader_error(GLuint shader_index) {
    int params = -1;
    glGetShaderiv(shader_index, GL_COMPILE_STATUS, &params);
    if (GL_TRUE != params) {
        std::cerr << "ERROR: GL shader index "
            << shader_index << " did not compile" << std::endl;
        int max_length = 2048;
        int actual_length = 0;
        char shader_log[2048];
        glGetShaderInfoLog(shader_index, max_length, &actual_length, shader_log);
        std::cerr << "shader info log:"
            << std::endl <<  shader_log;

        assert(GL_TRUE == params);
    }
}

inline void handle_link_error(GLuint programme) {
    // check if link was successful
    int params = -1;
    glGetProgramiv(programme, GL_LINK_STATUS, &params);
    if (GL_TRUE != params) {
        std::cerr <<
          "ERROR: could not link shader programme" << std::endl;
        int max_length = 2048;
        int actual_length = 0;
        char program_log[2048];
        glGetProgramInfoLog(programme, max_length, &actual_length, program_log);
        printf("program info log for GL index %u:\n%s", programme, program_log);
        assert(GL_TRUE == params);
    }
}
