#version 400

#define M_PI 3.1415926535897932384626433832795

in vec3 p0;
in vec3 p1;
in vec3 m0;
in vec3 m1;

in vec3 dir;
in vec3 start;
in vec3 end;

uniform mat4 mvp;
uniform ivec2 viewport_dimensions;
uniform vec3 position;
uniform vec3 lookat;
uniform vec3 cam_up;
uniform samplerCube skybox;

out vec4 frag_colour;

// XXX: move these to uniform?
const float track_diameter = 0.05;
const float track_width = 0.8;

// XXX: uniform
const float fov = 45.f;

const vec3 diffuse_color = vec3(0.25098, 0.26667, 0.32549);
const vec3 light_source = vec3(0.0, 0.0, -1.0);

vec3 interpolate_derivative(float t) {
    float t2 = pow(t, 2);
    return (6*t2-6*t)*p0
        + (3*t2-4*t + 1)*m0
        + (-6*t2 + 6*t)*p1
        + (3*t2-2*t)*m1;
}

// XXX: some normalize are probably too much
vec3 calculate_view_vector() {
    // uv ranges from -1 to +1
    vec2 uv = (-1.0 + 2.0*gl_FragCoord.xy / viewport_dimensions.xy) * vec2(float(viewport_dimensions.x)/viewport_dimensions.y, 1.0);

    // View vector
    vec2 pixel = uv.xy * tan(radians(fov/2));

    vec3 right = normalize(cross(lookat - position, cam_up));
    vec3 up = normalize(cam_up);

    vec3 rotated_pixel = right * pixel.x + up * pixel.y;
    vec3 v = normalize(lookat - position) + rotated_pixel;
    return v;
}

// https://github.com/glslify/glsl-smooth-min/blob/master/demo/lookat.glsl
mat3 calcLookAtMatrix(in vec3 camPosition, in vec3 camTarget, in float roll) {
    vec3 ww = normalize(camTarget - camPosition);
    vec3 uu = normalize(cross(ww, vec3(sin(roll), cos(roll), 0.0)));
    vec3 vv = normalize(cross(uu, ww));

    return transpose(mat3(uu, vv, ww));
}

// First three components are the normal,
// fourth component is the z-coordinate in cylinder axes.
vec4 intersect_cylinder(vec3 v) {
    // Compensate translation/rotation of cylinder
    vec3 s = position-start;
    mat3 M = calcLookAtMatrix(vec3(0), dir, 0.0);

    // Now `dir` is in the negative z,
    // while x,y, are in the cylinder plane
    s = M*s;
    v = M*v;

    float r = track_diameter/2;

    float a = v.x*v.x + v.y*v.y;
    float b = 2*(s.x*v.x+s.y*v.y);
    float c = s.x*s.x + s.y*s.y - r*r;

    float d = b*b-4*a*c;
    if (d < 0.0) return vec4(-1.0);
    float t0 = (-b+sqrt(d))/(2*a);
    float t1 = (-b-sqrt(d))/(2*a);

    // only keep the closest value (minimum, positive t)
    float t;
    if (max(t0, t1) < 0)  {
        return vec4(-1.0);
    }
    if (t0 < 0) {
        t = t1;
    }
    else if (t1 < 0) {
        t = t0;
    }
    else {
        t = min(t0, t1);
    }
    vec3 intersection = s + t*v;
    return vec4(inverse(M)*vec3(intersection.xy,0.0), intersection.z);
}

// http://www.neilmendoza.com/glsl-rotation-about-an-arbitrary-axis/
mat3 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c);
}

//  I - 2.0 * dot(N, I) * N.
//
//  OpenGL provides reflect(), but some GPU's don't like that.
vec3 my_reflect(vec3 incident, vec3 normal) {
    return incident - 2.0*dot(incident, normal) * normal;
}

void main() {
    vec3 v = calculate_view_vector();
    vec4 intersection = intersect_cylinder(v);
    if (intersection.w < 0 || intersection.w > length(dir)) {
        // To debug geometry, switch comments;
        // frag_colour = vec4(0, 0, 1, 1.0);
        // return;
        discard;
    }

    vec3 cylindrical_normal = normalize(vec3(intersection));
    // XXX: These should work, but apparently they don't.
    // vec3 tangent = normalize(interpolate_derivative(intersection.w/length(dir)));
    // float deviation = acos(dot(normalize(dir), tangent));
    // vec3 normal = rotationMatrix(cross(cylindrical_normal, tangent), deviation) * cylindrical_normal;

    float costheta = dot(cylindrical_normal, -light_source);

    // Environment map
    mat3 z_is_up = mat3(1,0,0,
                        0,0,1,
                        0,-1,0);
    vec3 texture_coordinate = z_is_up * my_reflect(v, cylindrical_normal);

    // Emphasize light coming from above
    float env_factor = pow(dot(normalize(texture_coordinate), vec3(0, 0, 1)), 2);
    vec3 env_color = vec3(texture(skybox, texture_coordinate));
    frag_colour = vec4(costheta*diffuse_color + env_factor*env_color, 1.0);

    // Calculate fragment depth
    vec3 in_eye_space = start + dir*intersection.w + cylindrical_normal;
    vec4 projected = mvp*vec4(in_eye_space, 1.0);
    projected = projected/projected.w;

    // frag_colour = vec4(vec3(projected.z), 1.0);
    gl_FragDepth = projected.z;
}
