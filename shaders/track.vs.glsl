#version 400

layout(location = 0) in vec3 vp;
layout(location = 1) in vec3 derivative;
layout(location = 2) in vec3 binormal;
uniform mat4 mvp;

out vec3 vPosition;
out vec3 vDerivative;
out vec3 vBinormal;

void main() {
    vPosition = vp;
    vDerivative = derivative;
    vBinormal = binormal;

    gl_Position = mvp*vec4(vp, 1.0);
}
