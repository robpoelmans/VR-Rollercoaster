#version 400

out vec4 frag_colour;
in float gTemperature;


vec4 blackbody_to_rgb(float temperature) {
    temperature /= 100;

    vec3 color = vec3(1.0);

    // Red
    if (temperature <= 66) {
        color.x = 255;
    } else {
        color.x = temperature - 60;
        color.x = 329.698727446 * pow(color.x, -0.1332047592);
    }

    // Green
    if (temperature <= 66) {
        color.y = temperature;
        color.y = 99.4708025861 * log(color.y) - 161.1195681661;
    } else {
        color.y = temperature - 60;
        color.y = 288.1221695283 * pow(color.y, -0.0755148492);
    }

    // Blue
    if (temperature >=66) {
        color.z = 255;
    } else if (temperature <= 19) {
        color.z = temperature - 10;
        color.z = 138.5177312231 * log(color.z) - 305.0447927307;
    }

    color = clamp(color, 0, 255);
    return vec4(color/256, 1.0);
}

void main() {
    frag_colour = blackbody_to_rgb(gTemperature);
}
