#version 400

layout(location = 0) in vec3 position;
layout(location = 1) in float temperature;
uniform mat4 mvp;

out vec3 vPosition;
out float vTemperature;

void main() {
    vPosition = position;
    vTemperature = temperature;

    gl_Position = mvp*vec4(position, 1.0);
}
