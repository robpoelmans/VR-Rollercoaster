#!/usr/bin/env bash

echo $@

name=${3//[\.\/]/_}
cat $1 | ( echo "unsigned char ${name}[] = {"; xxd -i; echo "};" ) > $2
# echo "unsigned char ${name}[];" > $1.h
