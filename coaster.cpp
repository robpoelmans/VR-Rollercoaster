#include <iostream>

#include <glm/matrix.hpp>
#include <glm/ext.hpp>

#include "context.hpp"
#include "coaster.hpp"
#include "rollercoaster-mechanics.hpp"
#include "shaders/shaders.hpp"

coaster::coaster(rollercoaster_mechanics *_mech)
    : position(0) {
    this->mech = _mech;

    generate_vertices();

    const char* vertex_shader = (const char*)track_vs;
    const char* fragment_shader = (const char*)track_fs;
    const char* geometry_shader = (const char*)track_ge;

    vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vertex_shader, NULL);
    glCompileShader(vs); handle_shader_error(vs);
    fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fragment_shader, NULL);
    glCompileShader(fs); handle_shader_error(fs);
    ge = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(ge, 1, &geometry_shader, NULL);
    glCompileShader(ge); handle_shader_error(ge);

    shader_programme = glCreateProgram();
    glAttachShader(shader_programme, vs);
    glAttachShader(shader_programme, ge);
    glAttachShader(shader_programme, fs);
    glLinkProgram(shader_programme);
    handle_link_error(shader_programme);

    this->u_mvp = glGetUniformLocation(shader_programme, "mvp");
    this->u_viewport_dimensions = glGetUniformLocation(shader_programme, "viewport_dimensions");
    this->u_position = glGetUniformLocation(shader_programme, "position");
    this->u_lookat = glGetUniformLocation(shader_programme, "lookat");
    this->u_up = glGetUniformLocation(shader_programme, "cam_up");
    assert(this->u_mvp != -1);
}

void coaster::generate_vertices() {
    auto push = [this](glm::vec3 v, glm::vec3 d, glm::vec3 b) {
        vertices.push_back(v.x);
        vertices.push_back(v.y);
        vertices.push_back(v.z);

        derivatives.push_back(d.x);
        derivatives.push_back(d.y);
        derivatives.push_back(d.z);

        binormals.push_back(b.x);
        binormals.push_back(b.y);
        binormals.push_back(b.z);
    };

    float max_segment_length = 0.5;

    size_t j = 0;
    for(auto &vert: this->mech->get_track()) {
        glm::vec3 position(vert.x, vert.y, vert.z);
        glm::vec3 derivative(vert.dx, vert.dy, vert.dz);
        glm::vec3 binormal = mech->track_binormal(j);
        push(position, derivative, binormal);
        if (vert.segment_length < max_segment_length) {
            ++j; continue;
        }

        size_t count = ceil(vert.segment_length / max_segment_length);
        for (size_t i = 1; i < count; ++i) {
            float t = float(i)/count;
            push(mech->track_at(t + j),
                    mech->track_derivative_at(t + j),
                    mech->track_binormal(t + j));
        }
        ++j;
    }

    glGenBuffers(1, &position_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &*vertices.begin(), GL_STATIC_DRAW);

    glGenBuffers(1, &derivative_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, derivative_vbo);
    glBufferData(GL_ARRAY_BUFFER, derivatives.size() * sizeof(float), &*derivatives.begin(), GL_STATIC_DRAW);

    glGenBuffers(1, &binormal_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, binormal_vbo);
    glBufferData(GL_ARRAY_BUFFER, binormals.size() * sizeof(float), &*binormals.begin(), GL_STATIC_DRAW);

    glGenVertexArrays(1, &track_vao);
    glBindVertexArray(track_vao);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, derivative_vbo);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, binormal_vbo);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
}

void coaster::view(context *ctx, float dx, float dy) {
    float position = mech->get_position();

    ctx->position = mech->track_at(position);
    ctx->lookat = mech->track_at(position+0.1);
    ctx->up = mech->track_normal(position);

    ctx->lookat = ctx->position +
        glm::rotate(
                glm::rotate(glm::normalize(ctx->lookat - ctx->position), -dy, mech->track_binormal(position)),
                -dx, ctx->up);

    // Top down:
    // ctx->position = glm::vec3(13, -20, 180);
    // ctx->lookat = glm::vec3(13, -20, 0);
    // ctx->up = glm::vec3(0,1,0);
    // South view:
    // ctx->position = glm::vec3(13, -180, 20);
    // ctx->lookat = glm::vec3(13, 0, 20);
    // ctx->up = glm::vec3(0,0,1);

    ctx->view = glm::lookAt(ctx->position, ctx->lookat, ctx->up);
    ctx->skybox_view = glm::lookAt(glm::vec3(0,0,0), ctx->lookat-ctx->position, ctx->up);
}

void coaster::draw(const context &context) {
    glUseProgram(shader_programme);
    glm::mat4 model = glm::mat4(1.0f);
    glm::mat4 mvp = context.camera_matrix * model;
    glUniformMatrix4fv(this->u_mvp, 1, GL_FALSE, &mvp[0][0]);
    glUniform2iv(this->u_viewport_dimensions, 1, &context.viewport_dimensions[0]);
    glUniform3fv(this->u_position, 1, &context.position[0]);
    glUniform3fv(this->u_lookat , 1, &context.lookat[0]);
    glUniform3fv(this->u_up , 1, &context.up[0]);
    glBindTexture( GL_TEXTURE_CUBE_MAP, context.skybox_texture );

    glActiveTexture(GL_TEXTURE0);

    glBindVertexArray(track_vao);
    glDrawArrays(GL_LINE_LOOP, 0, vertices.size()/3);
}
