#include <iostream>
#include <array>

#include <glm/matrix.hpp>
#include <glm/ext.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "context.hpp"
#include "skybox.hpp"
#include "rollercoaster-mechanics.hpp"
#include "shaders/shaders.hpp"
#include "textures/textures.hpp"

const std::array<float,108> vertices = {
    -10.0f,  10.0f, -10.0f,
    -10.0f, -10.0f, -10.0f,
     10.0f, -10.0f, -10.0f,
     10.0f, -10.0f, -10.0f,
     10.0f,  10.0f, -10.0f,
    -10.0f,  10.0f, -10.0f,

    -10.0f, -10.0f,  10.0f,
    -10.0f, -10.0f, -10.0f,
    -10.0f,  10.0f, -10.0f,
    -10.0f,  10.0f, -10.0f,
    -10.0f,  10.0f,  10.0f,
    -10.0f, -10.0f,  10.0f,

     10.0f, -10.0f, -10.0f,
     10.0f, -10.0f,  10.0f,
     10.0f,  10.0f,  10.0f,
     10.0f,  10.0f,  10.0f,
     10.0f,  10.0f, -10.0f,
     10.0f, -10.0f, -10.0f,

    -10.0f, -10.0f,  10.0f,
    -10.0f,  10.0f,  10.0f,
     10.0f,  10.0f,  10.0f,
     10.0f,  10.0f,  10.0f,
     10.0f, -10.0f,  10.0f,
    -10.0f, -10.0f,  10.0f,

    -10.0f,  10.0f, -10.0f,
     10.0f,  10.0f, -10.0f,
     10.0f,  10.0f,  10.0f,
     10.0f,  10.0f,  10.0f,
    -10.0f,  10.0f,  10.0f,
    -10.0f,  10.0f, -10.0f,

    -10.0f, -10.0f, -10.0f,
    -10.0f, -10.0f,  10.0f,
     10.0f, -10.0f, -10.0f,
     10.0f, -10.0f, -10.0f,
    -10.0f, -10.0f,  10.0f,
     10.0f, -10.0f,  10.0f
};

skybox::skybox(context &context) {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &*vertices.begin(), GL_STATIC_DRAW);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    const char* vertex_shader = (const char*)skybox_vs;
    const char* fragment_shader = (const char*)skybox_fs;

    vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vertex_shader, NULL);
    glCompileShader(vs); handle_shader_error(vs);
    fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fragment_shader, NULL);
    glCompileShader(fs); handle_shader_error(fs);

    shader_programme = glCreateProgram();
    glAttachShader(shader_programme, fs);
    glAttachShader(shader_programme, vs);
    glLinkProgram(shader_programme);
    handle_link_error(shader_programme);

    this->skyboxView = glGetUniformLocation(shader_programme, "skyboxView");
    assert(this->skyboxView != -1);
    this->skyboxProjection = glGetUniformLocation(shader_programme, "skyboxProjection");
    assert(this->skyboxProjection != -1);

    //  load textures   ---   possibly in skybox class?
    std::vector<const GLchar *> faces;
    faces.push_back("../resources/skybox/SunSetRight2048.png");
    faces.push_back("../resources/skybox/SunSetLeft2048.png");
    faces.push_back("../resources/skybox/SunSetDown2048.png");
    faces.push_back("../resources/skybox/SunSetUp2048.png");
    faces.push_back("../resources/skybox/SunSetBack2048.png");
    faces.push_back("../resources/skybox/SunSetFront2048.png");

    cubemapTexture = TextureLoading::LoadCubemap(faces);
    assert(cubemapTexture != 0);

    context.skybox_texture = cubemapTexture;
}

void skybox::draw(const context &context) {
    glUseProgram(shader_programme);
    glUniformMatrix4fv(this->skyboxProjection,1,GL_FALSE, glm::value_ptr(context.projection));
    glUniformMatrix4fv(this->skyboxView,1,GL_FALSE, glm::value_ptr(context.skybox_view));

    glBindVertexArray(vao);
    glBindTexture( GL_TEXTURE_CUBE_MAP, cubemapTexture );
    glDrawArrays(GL_TRIANGLES,0,vertices.size()/3);
}
