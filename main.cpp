#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/matrix.hpp>
#include <glm/ext.hpp>

#include <IL/il.h>

#include "shaders/shaders.hpp"
#include "textures/textures.hpp"
#include "rollercoaster-mechanics.hpp"
#include "coaster.hpp"
#include "skybox.hpp"
#include "context.hpp"
#include "flamethrower.hpp"

context drawing_context;
double prevX = 0.0f;
double prevY = 0.0f;
float dx = 0.0f;
float dy = 0.0f;
const float deg = M_PI/2048;

static void cursorPositionCallback(GLFWwindow *window, double xPos, double yPos) {
  dx += xPos - prevX;
  dy += yPos - prevY;
  // std::cout << dx << " - " << dy << std::endl;

  prevX = xPos;
  prevY = yPos;
};

void window_size_callback(GLFWwindow *window, int width, int height) {
    glfwSetWindowSize(window, width, height);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    drawing_context.viewport_dimensions = glm::ivec2(width, height);
    glViewport(0, 0, width, height);
}

int main() {
    GLFWwindow* window;

    /* Initialize the library */
    if (glfwInit() == 0) {
        std::cerr << "Could not initialize glfw" << std::endl;
        return -1;
    }

    /* Create a windowed mode window and its OpenGL context */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    window = glfwCreateWindow(1024, 768, "VR project", nullptr, nullptr);
    drawing_context.viewport_dimensions = glm::ivec2(1024, 768);
    if (window == nullptr)
    {
        std::cerr << "Could not create glfw window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwGetCursorPos(window, &prevX, &prevY);

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    assert(glewInit() == GLEW_OK);

    const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
    const GLubyte* version = glGetString(GL_VERSION); // version as a string
    std::cerr << "Renderer:" << renderer << std::endl;
    std::cerr << "OpenGL version supported " << version << std::endl;

    // Enable DevIL
    ilInit();

    glEnable(GL_DEPTH_TEST); // enable depth-testing

    auto mech = rollercoaster_mechanics::load("../coaster.roll");
    coaster mCoaster(&mech);
    mech.start();

    skybox mySkybox(drawing_context);
    flamethrower mFlamethrower;

    /* Loop until the user closes the window */
    while (glfwWindowShouldClose(window) == 0)
    {
        mech.update();
        mFlamethrower.update();

        glm::mat4 projection = glm::perspective(glm::radians(45.0f),
                float(drawing_context.viewport_dimensions.x)/float(drawing_context.viewport_dimensions.y), 0.1f, 500.0f);
        mCoaster.view(&drawing_context, dx*deg, dy*deg);
        drawing_context.projection = projection;

        drawing_context.camera_matrix = projection * drawing_context.view;
        drawing_context.skybox_view = drawing_context.skybox_view;

        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glDepthFunc(GL_LESS);
        mCoaster.draw(drawing_context);
        mFlamethrower.draw(drawing_context);

        glDepthFunc(GL_LEQUAL);
        mySkybox.draw(drawing_context);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
