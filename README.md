# VR project

## Prerequisites:

- Mesos/ninja build system
- GLFW

## Building

```
meson build
cd build
ninja
./vr
