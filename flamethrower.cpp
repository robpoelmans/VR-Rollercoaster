#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "flamethrower.hpp"
#include "shaders/shaders.hpp"

flamethrower::flamethrower() {
    previous_time = glfwGetTime();

    const char* vertex_shader = (const char*)flamethrower_vs;
    const char* fragment_shader = (const char*)flamethrower_fs;
    const char* geometry_shader = (const char*)flamethrower_ge;

    vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vertex_shader, NULL);
    glCompileShader(vs); handle_shader_error(vs);
    fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fragment_shader, NULL);
    glCompileShader(fs); handle_shader_error(fs);
    ge = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(ge, 1, &geometry_shader, NULL);
    glCompileShader(ge); handle_shader_error(ge);

    shader_programme = glCreateProgram();
    glAttachShader(shader_programme, vs);
    glAttachShader(shader_programme, ge);
    glAttachShader(shader_programme, fs);
    glLinkProgram(shader_programme);
    handle_link_error(shader_programme);

    this->u_mvp = glGetUniformLocation(shader_programme, "mvp");

    // Set all particles
    for(size_t i = 0; i < max_particles; ++i) {
        position[3*i] = 0;
        position[3*i+1] = 0;
        position[3*i+2] = 0;
        temperature[i] = 0;
        velocity[i] = glm::vec3(0);
    }

    glGenBuffers(1, &vbo);
    glGenBuffers(1, &temperature_buffer);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(position), position, GL_STREAM_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, temperature_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(temperature), temperature, GL_STREAM_DRAW);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, temperature_buffer);
    glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 0, nullptr);
}

float random_float(float max) {
    return static_cast <float> (rand()) / static_cast <float> (RAND_MAX/max);
}

void flamethrower::respawn(const size_t i) {
    position[3*i + 0] = random_float(0.2)-0.1;
    position[3*i + 1] = random_float(0.2)-0.1;
    position[3*i + 2] = random_float(0.2);

    velocity[i] = glm::vec3(random_float(0.6)-0.3, random_float(0.6)-0.3, random_float(2)+2);
    temperature[i] = random_float(6000)+2000;
}

void flamethrower::update() {
    double now = glfwGetTime();
    double delta_time = now - previous_time;

    const float friction = 0.1;
    const float temperature_decay = 1;
    const float buoyancy_coef = 0.00005;

    for(size_t i = 0; i < max_particles; ++i) {
        if(temperature[i] < 500) {
            respawn(i);
            continue;
        }

        position[3*i + 0] += velocity[i].x*delta_time;
        position[3*i + 1] += velocity[i].y*delta_time;
        position[3*i + 2] += velocity[i].z*delta_time;
        velocity[i] -= glm::vec3(friction*delta_time)*velocity[i];
        float buoyancy = buoyancy_coef*(temperature[i] - 300);
        velocity[i] -= glm::vec3(0, 0, 9.81*delta_time - buoyancy);
        temperature[i] -= temperature[i] - temperature[i] * exp(-temperature_decay * delta_time);
    }

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, max_particles * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, max_particles * 4 * sizeof(GLfloat), position);

    glBindBuffer(GL_ARRAY_BUFFER, temperature_buffer);
    glBufferData(GL_ARRAY_BUFFER, max_particles * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, max_particles * sizeof(GLfloat), temperature);

    previous_time = now;
}

void flamethrower::draw(const context &context) {
    glUseProgram(shader_programme);
    glm::mat4 model = glm::mat4(1.0f);
    glm::mat4 mvp = context.camera_matrix * model;
    glUniformMatrix4fv(this->u_mvp, 1, GL_FALSE, glm::value_ptr(mvp));

    glBindVertexArray(vao);
    glDrawArrays(GL_POINTS, 0, sizeof(position)/3);
}
