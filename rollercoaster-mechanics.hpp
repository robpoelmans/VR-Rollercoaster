#pragma once

#include <string>
#include <vector>

#include <glm/glm.hpp>

class file_not_found_exception : public std::exception {
};

struct coordinate {
    float x, y, z;
};

// flags
namespace segment_flag {
    const uint8_t z = 1;
    const uint8_t o = 2;
    const uint8_t i = 4;
}

struct base_coordinate {
    float x, y, z;
    float dx, dy, dz;
    uint8_t flag;
    float rot_begin;
    float rot_end;
    float segment_length;
};

class rollercoaster_mechanics {
    std::vector<base_coordinate> track;

    void precalculate_partial_lengths();

    bool running;
    float position;
    float velocity;
    double previous_time;
public:
    static rollercoaster_mechanics load(const std::string &path);

    std::vector<base_coordinate> &get_track() {
        return this->track;
    }
    glm::vec3 track_at(float theta);
    glm::vec3 track_derivative_at(float theta);
    glm::vec3 track_tangent(float theta);
    glm::vec3 track_normal(float theta);
    glm::vec3 track_binormal(float theta);
    size_t track_segment_count() {
        return this->track.size();
    }
    float get_position() {
        return position;
    }
    float track_length();
    float interval_length(float start, float end);
    void start();
    void update();
};
