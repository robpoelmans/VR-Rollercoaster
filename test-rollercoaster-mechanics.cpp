#include "rollercoaster-mechanics.hpp"

#include <iostream>

int main() {
    auto coaster = rollercoaster_mechanics::load("../coaster.roll");

    for(float theta = 0; theta < coaster.track_segment_count(); theta += 0.1) {
        auto pos = coaster.track_at(theta);
        std::cout << pos.x << "\t" << pos.z << std::endl;
    }

    std::cout << std::endl;
    std::cout << "Track length: " << coaster.track_length() << std::endl;
    return 0;
}
