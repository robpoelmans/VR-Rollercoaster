FROM fedora:latest

RUN dnf install -y meson ninja-build glfw-devel mesa-libGL-devel libGLEW glew-devel glm-devel DevIL-devel
RUN dnf groupinstall -y "C Development Tools and Libraries"
