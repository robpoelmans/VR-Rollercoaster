#pragma once

#include <cassert>
#include <vector>

#include <IL/il.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class TextureLoading {
public:
    static GLuint LoadTexture (const GLchar *path) {
        //  load, create texture and create mipmaps
        unsigned int imageID;
        ilGenImages(1, &imageID);
        ILboolean success;

        ilBindImage(imageID);
        ilEnable(IL_ORIGIN_SET);
        ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
        success = ilLoadImage((ILstring)path);
        assert(success);

        GLuint textureID;

        glGenTextures(1,&textureID);
        glBindTexture(GL_TEXTURE_2D,textureID);

        //  set texture params
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);

        //  set texture filtering
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

        ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                ilGetInteger(IL_IMAGE_WIDTH),ilGetInteger(IL_IMAGE_HEIGHT),
                0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());
        glGenerateMipmap(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,0);

        ilDeleteImages(1, &imageID);

        return textureID;
    }

    static GLuint LoadCubemap( std::vector<const GLchar *> faces) {
        GLuint textureID;
        ILboolean success;

        unsigned int imageIDs[6];
        ilGenImages(6, imageIDs);

        glGenTextures(1,&textureID);
        glBindTexture(GL_TEXTURE_CUBE_MAP,textureID);

        //  set texture params
        glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_EDGE);

        //  set texture filtering
        glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

        //  load, create texture and create mipmaps
        for(GLuint i = 0; i < faces.size(); i++) {
            ilBindImage(imageIDs[i]);
            ilEnable(IL_ORIGIN_SET);
            ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
            success = ilLoadImage((ILstring)faces[i]);
            assert(success);
            ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);

            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,0,GL_RGB, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());
        }
        glBindTexture(GL_TEXTURE_CUBE_MAP,0);

        ilDeleteImages(6, imageIDs);

        return textureID;
    }
};
