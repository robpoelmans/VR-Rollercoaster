#include "rollercoaster-mechanics.hpp"

#include <iostream>
#include <cmath>

#include <glm/gtx/rotate_vector.hpp>
#include <GLFW/glfw3.h>

const float epsilon = 0.001;
const float gravitational_constant = 9.81;
const float mass = 1;
const float linear_friction = 0.02;

// Cubic spline interpolation
inline float interpolate(float p0, float p1, float m0, float m1, float t) {
    float t3 = std::pow(t, 3.0f);
    float t2 = std::pow(t, 2.0f);
    return (2*t3-3*t2+1)*p0
        + (t3-2*t2 + t)*m0
        + (-2*t3 + 3*t2)*p1
        + (t3-t2)*m1;
}

inline float interpolate_derivative(float p0, float p1, float m0, float m1, float t) {
    float t2 = std::pow(t, 2.0f);
    return (6*t2-6*t)*p0
        + (3*t2-4*t + 1)*m0
        + (-6*t2 + 6*t)*p1
        + (3*t2-2*t)*m1;
}

inline float interpolate_linear(float p0, float p1, float theta) {
    return (1- theta)* p0 + theta*p1;
}

inline float sign(float a) {
    if(a < 0) return -1;
    return 1;
}

glm::vec3 rollercoaster_mechanics::track_at(float theta) {
    glm::vec3 interpolated;

    size_t i = std::floor(theta);
    theta = theta - i;

    auto &current = this->track[i % this->track.size()];
    auto &next = this->track[(i+1) % this->track.size()];

    interpolated.x = interpolate(current.x, next.x, current.dx, next.dx, theta);
    interpolated.y = interpolate(current.y, next.y, current.dy, next.dy, theta);
    interpolated.z = interpolate(current.z, next.z, current.dz, next.dz, theta);

    return interpolated;
}

glm::vec3 rollercoaster_mechanics::track_derivative_at(float theta) {
    glm::vec3 interpolated;

    size_t i = std::floor(theta);
    theta = theta - i;

    auto &current = this->track[i % this->track.size()];
    auto &next = this->track[(i+1) % this->track.size()];

    interpolated.x = interpolate_derivative(current.x, next.x, current.dx, next.dx, theta);
    interpolated.y = interpolate_derivative(current.y, next.y, current.dy, next.dy, theta);
    interpolated.z = interpolate_derivative(current.z, next.z, current.dz, next.dz, theta);

    return interpolated;
}

glm::vec3 rollercoaster_mechanics::track_tangent(float theta) {
    return glm::normalize(track_derivative_at(theta));
}
glm::vec3 rollercoaster_mechanics::track_normal(float theta) {
    auto current = track_tangent(theta);
    glm::vec3 binorm;
    size_t i = std::floor(theta);
    auto &segment = this->track[i];
    auto flags = segment.flag;

    if(flags & segment_flag::z) {
        binorm = glm::cross(current, glm::vec3(0,0,1));
    } else if(flags & (segment_flag::i | segment_flag::o)) {
        auto next = track_derivative_at(theta + 0.001);
        // Reuse the previous normal if we overflow a segment
        if(theta + epsilon > i+1) {
            next = current;
            current = track_derivative_at(theta - 0.001);
        }
        binorm = glm::cross(current, next);
        if (flags & segment_flag::o) {
            binorm = -binorm;
        }
    }

    // rotate
    float angle = interpolate(segment.rot_begin * M_PI, segment.rot_end * M_PI, 0, 0, theta - i);

    glm::vec3 norm = glm::normalize(glm::cross(binorm, current));
    return glm::rotate(norm, angle, track_tangent(theta));
}
glm::vec3 rollercoaster_mechanics::track_binormal(float theta) {
    auto cr = glm::cross(track_tangent(theta),track_normal(theta));
    assert(glm::length(cr) != 0);
    return cr;
}

float rollercoaster_mechanics::track_length() {
    return interval_length(0, this->track_segment_count());
}

float rollercoaster_mechanics::interval_length(const float start, const float end) {
    const float h = 0.0001;
    float len = 0;

    float pos = start;
    while(pos < end) {
        auto d = this->track_derivative_at(pos);
        float sum_of_squares = d.x*d.x + d.y*d.y + d.z*d.z;
        len += std::sqrt(sum_of_squares)*h;
        pos += h;
    }

    return len;
}

void rollercoaster_mechanics::precalculate_partial_lengths() {
    for(size_t i = 0; i < track_segment_count(); ++i) {
        track[i].segment_length = interval_length(i, i+1);
    }
}

void rollercoaster_mechanics::start() {
    this->previous_time = glfwGetTime();
    this->running = true;
}

void rollercoaster_mechanics::update() {
    const double now = glfwGetTime();
    const double dt = now - previous_time;
    const float previous_position = position;
    float total_dx;

    // x = x+v*dt
    {
        auto &previous_segment = track[floor(position)];
        float dx = velocity*dt;
        total_dx = dx;

        float t_per_meter = 1. / previous_segment.segment_length;
        float delta_t = t_per_meter * dx;

//         while(position - floor(position) + delta_t > 1) {
// #ifdef DEBUG
//             std::cout << "Overflowing segment" << std::endl;
// #endif
//             // If we overflow the segment, go to the end of the segment,
//             // and start over
//             dx -= interval_length(position, ceil(position));
//             position = ceil(position);
//
//             previous_segment = track[position];
//             t_per_meter = 1. / previous_segment.segment_length;
//             delta_t = t_per_meter * dx;
//         }
        position += delta_t;
        if(position > track_segment_count()) {
            position -= track_segment_count();
        }

#ifdef DEBUG
        float error = interval_length(previous_position, position)-total_dx;
        error /= interval_length(previous_position, position);
        error = abs(error);
        if (error > 0.1) { // Print a warning for more that 10% of error
            std::cerr << "W: error is " << error *100 << "%" << std::endl;
        }
#endif
    }

    // Correct velocity (using kinetic energy + gravitational potential = ct)
    float velocity_sign = sign(velocity);
    float e_kin = mass*velocity*velocity/2;
    auto displacement = track_at(position) - track_at(previous_position);
    float e_grav = gravitational_constant*mass*displacement.z; // Positive when climbing
    e_kin -= e_grav;
    e_kin -= abs(linear_friction * velocity * total_dx);
    float energy_sign = sign(e_kin);
    e_kin = abs(e_kin);
    velocity = velocity_sign*energy_sign*sqrt(2*e_kin/mass);

    // std::cout << "v: " << velocity << std::endl;

    // Set time for next round
    this->previous_time = now;
}
