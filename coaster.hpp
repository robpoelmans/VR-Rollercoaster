#pragma once

#include <vector>

#include <GL/glew.h>
#include <glm/ext.hpp>

class rollercoaster_mechanics;
struct context;
class coaster {
    GLuint vs, fs, ge;
    GLuint position_vbo;
    GLuint derivative_vbo;
    GLuint binormal_vbo;
    GLuint track_vao;
    GLuint shader_programme;

    // Uniforms
    GLint u_mvp;
    GLint u_viewport_dimensions;
    GLint u_position;
    GLint u_lookat;
    GLint u_up;

    float position;

    rollercoaster_mechanics *mech;

    std::vector<float> vertices;
    std::vector<float> derivatives;
    std::vector<float> binormals;

    void generate_vertices();
public:
    coaster(rollercoaster_mechanics *);
    void draw(const context &);
    void view(context *ctx, float dx, float dy);
};
