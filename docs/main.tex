%!TEX program=xelatex
%!TEX root=main.tex
\documentclass{article}
\usepackage{vub}
\usepackage{physics}
\usepackage{minted}
\usepackage[toc,page]{appendix}

\author{Ruben De Smet, Rob Stefan Poelmans}
\title{Report VR project}
\faculty{sciences and bioengineering sciences}

\begin{document}
\maketitle

\begin{abstract}
  A simple roller coaster based on The Swarm (Thorpe Park)
  was built and rendered in OpenGL 4.0.
  This report details the design decisions, and the implementation.
\end{abstract}


\section{Introduction}%
\label{sec:introduction}

We chose the Thorpe~Park based roller coaster called ``The Swarm'' as the base for our modelling,
because of the potential features we could implement.
``The Swarm'' has four inversions, and its track is approximately 750\,m long.
% TODO: this is a crappy sentence
Among the scenery elements, there is a flame thrower, which makes a cool feature to implement in the OpenGL project.

Implemented features:

\begin{itemize}
  \item A sky box;
  \item A simple ray caster for the track, with both diffuse lighting and an environment map coming from the sky box;
  \item A cubic spline-defined track, based on imagery of ``The Swarm''.
\end{itemize}

\section{The track}%
\label{sec:the_track}

The track is modelled as a cubic spline, defined at some points.
Those anchor points are chosen where all (or most of) the track data is know;
for example at the top of a loop (where \(\dv{\vec{p}}{z}=0\)), or at the station.

Track data comes from Google Maps' satellite imagery, and from Thorpe Parks build planning application with the local government.

The track slope is modelled as a simple linear interpolation, although here too a cubic spline would be smoother.

Apart from simplicity, another reason to use cubic splines is because their derivative is defined in every point.
This means that the tangent, normal and binormal are defined in every point, making shading smoother.


\subsection{Rendering the track}%
\label{sub:rendering_the_track}

Initially, we crudely rendered the track by segmenting it into cubes,
and feeding it into OpenGL using a \mintinline{C}{GL_STATIC_DRAW};

The fragment shader assigned a static grey colour to the track, which was our first crude implementation.

\bigskip

In a second phase, the segmentation was handed off to a geometry shader.
This turned out to be fairly uninteresting, since the geometry shader is limited in the amount of vertices it may produce.

\bigskip

In a third refactor, the segmentation was put back into the client (C++),
which now only pushes interpolated track coordinates (and their derivatives and slope) into OpenGL.

The geometry shader now generates two boxes around every track segment (a left and a right rail).
The interesting part now lies in the fragment shader,
which calculates the intersection between the ray at the fragment's coordinate and the cylinder comprised in the box.

If the intersection is void (which is equivalent with a discriminant \(D<0\)),
the fragment is discarded.
If the intersection is non-void, the closest (with respect to the camera) intersection is chosen,
and its normal on the cylinder is calculated.
This normal is then used for both an environment map and diffuse lighting:

\inputminted[firstline=151,lastline=153]{glsl}{../shaders/track.fs.glsl}

\bigskip

We did consider calculating the intersection of the ray with the cubic spline,
but this yields a rational equation (with a polynomial of the 10th degree at worst),
which is only slowly solvable using iterative methods.

\subsection{Mechanics}%
\label{sub:mechanics}

For the roller coaster's mechanics, a very simple model is used and discretised:

\[\vec{F}=m\cdot\vec{a}=m\vec{g}-\mu |\vec{v}|\cdot\vec{1_t} + \vec{N}\]

where \(\vec{N}\) is the normal force that keeps the car on the track.
Thus, we assume a friction model, linear with the magnitude of the velocity.

At each time step, we calculate
\begin{align*}
  \Delta s &= |\vec{v_i}|*\Delta t\\
  E_{kin}  &= \frac{1}{2}mv_i^2 - mg\Delta z - \mu v \Delta s\\
  v_{i+1}  &= \sqrt{\frac{2E_{kin}}{m}}
\end{align*}

and map \(\Delta s\) and \(v_{i+1}\) back onto the cubic spline.

This mapping is non-trivial though, since the arc length is the integral

\begin{align*}
  s = \int_0^t \sqrt{\dv{p_x}{t}^2 + \dv{p_y}{t}^2 + \dv{p_z}{t}^2} \text{d}t
\end{align*}

and thus the inverse function \(t(s)\) demands a complex calculation.
To overcome this problem, we made a simple linear interpolation,
by calculating the spline's segment lengths (using a discretised version of the above integral),
and assuming a linear relation per segment.

The friction coefficient \(\mu\) was found through trial and error,
by tuning it as such that the maximum velocity was near the real roller coaster (95\,km/h),
and such that it could still go through all of the track.

\begin{appendices}
\section{Software stack and build instructions}
Our build tool of choice is \texttt{meson} combined with \texttt{ninja},
which are comparable to respectively \texttt{autotools} and \texttt{make}.

In order to build the project, create a build directory using

\begin{minted}{bash}
  meson build && cd build
\end{minted}

and start the build using

\begin{minted}{bash}
  ninja
\end{minted}

The executable \texttt{vr} will then exist in the build directory.

\subsection{Other dependencies}
\label{sub:other_dependencies}
As for libraries, we use
\begin{description}
  \item[glfw3] as windowing toolkit
  \item[glew] as extension loader
  \item[DevIL] as image file decoding and loading library
\end{description}

We also modestly assume the C++ compiler supports C++11 or above; most modern compilers nowadays support C++14 or C++17.
\end{appendices}

\section{Issues}%
\label{sec:issues}
During development we encountered several issues, some more challenging than others.

\subsection{SOIL}%
\label{sub:soil}
SOIL is an image library for OpenGL written in C.
We planned on using this library for loading textures.
Due to unresolved issues we looked into SOIL2, a more recent fork of SOIL.
This library was not easily integrated in our build system,
so finally we settled on DevIL, an image library with packages for both Arch Linux and macOS through homebrew.

\subsection{Ray casting a cubic spline}
\label{sub:raycasting_a_cubic_spline}

We first wanted to do the ray casting on an unsegmented (continuous) cubic spline.
The equations for this are (with \(\vec{p}(t)\) the spline):

\begin{align*}
  \vec{p}(t) + \vec{r} = \vec{s} + k\vec{v}\\
  \dv{\vec{p}(t)}{t}\cdot\vec{r} = 0\\
  |\vec{r}| = r
\end{align*}

where \(\vec{r}\) is the radius of the circular profile of the track.
These equations yield a very convoluted non-polynomial (rational) equation to be solved, for each fragment:

\begin{align*}
  r^2  &= |\vec{s}+k(t)\vec{v}-\vec{p}(t)|^2\\
  k(t) &= \frac{\sum\left(\dv{\vec{p_i}(t)}{t}p_i(t) - s_i\right)}{\sum\dv{\vec{p}(t)}{t}v_i}
\end{align*}

where to sum loops over three dimension \(x,y,z\).
For this reason, we settled on the cylindrical ray casting.

It should however be noted that it should be possible to map the cylindrical intersection back to the cubic spline,
to calculate the normal, and as such get a smoother diffuse lighting effect, and a more continuous environment mapping.

\subsection{Ray casting}%
\label{sub:ray_casting}

During the implementation of the ray casting algorithm,
the calculation of the view direction of the fragment is off by a small amount,
and it is yet to be determined why this is the case.

The relevant code is listed here:

\inputminted[firstline=42,lastline=55]{glsl}{../shaders/track.fs.glsl}

\end{document}
